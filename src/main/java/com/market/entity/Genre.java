package com.market.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.GenericGenerator;





@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity

@Table(name = "Market_GENRE")
public class Genre {
    @Id
//    @GenericGenerator(name = "Market_Genre_SEQUENCE", strategy="org.hibernate.id.enhanced.SequenceStyleGenerator",
//            parameters ={
//                    @org.hibernate.annotations.Parameter(name="Market_GENRE_SEQUENCE",value="Market_GENRE_SEQUENCE"),
//                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
//                    @org.hibernate.annotations.Parameter(name="increment_size",value="1")
//            })
//   @GeneratedValue( generator = "Market_GENRE_SEQUENCE")
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name = "genre_id")
    private Long id;
    private String name;
    
    @CreationTimestamp
    @Column(name = "createDateTime", nullable = false, updatable = false)
    private LocalDateTime createDateTime;
    @UpdateTimestamp
    private LocalDateTime updateDateTime;

}
