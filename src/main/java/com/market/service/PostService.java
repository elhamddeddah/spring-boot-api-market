package com.market.service;


import com.market.entity.Post;

import java.util.List;
import java.util.Map;

public interface PostService {

	Post getPost(long PostId);
	void deletePost(long PostId);
	List<Post> getAllPost();
//	List<Post> fetchAllPostByStatus(String status,int quantities);
	Post updatePost(Post Post);
//	public List<Post> findByQuantityLessThanEqualSeuil();
     Post createPost(Post Post);
//	Post validationPost(long PostId,String status);

}
