package com.market.service;

import com.market.entity.Genre;

import java.util.List;
import java.util.Map;

public interface GenreService {

	Genre getGenre(long GenreId);
	void deleteGenre(long GenreId);
	List<Genre> getAllGenre();
//	List<Genre> fetchAllGenreByStatus(String status,int quantities);
	Genre updateGenre(Genre Genre);
//	public List<Genre> findByQuantityLessThanEqualSeuil();
     Genre createGenre(Genre Genre);
//	Genre validationGenre(long GenreId,String status);

}
