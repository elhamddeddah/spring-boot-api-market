package com.market.controller;



import java.util.List;

import com.market.entity.Genre;
import com.market.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GenreMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//mark class as Controller
@RestController
@RequestMapping(value = "genre")
public class GenreController
{
    //autowire the GenreService class
    @Autowired
    GenreService genreService;
    //creating a get mapping that retrieves all the genre detail from the database
    @GetMapping()
    private List<Genre> getAllGenre()
    {
        return genreService.getAllGenre();
    }
    //creating a get mapping that retrieves the detail of a specific genre
    @GetMapping("/{genreid}")
    private Genre getGenre(@PathVariable("genreid") int genreid)
    {
        return genreService.getGenre(genreid);
    }
    //creating a delete mapping that deletes a specified genre
    @DeleteMapping("/{genreid}")
    private void deleteBook(@PathVariable("genreid") int genreid)
    {
        genreService.deleteGenre(genreid);
    }
    //creating genre mapping that genre the genre detail in the database
    @PostMapping()
    private Genre saveBook(@RequestBody Genre genre)
    {

        return genreService.updateGenre(genre);
    }
    //creating put mapping that updates the genre detail
    @PutMapping()
    private Genre update(@RequestBody Genre genre)
    {
        return genreService.createGenre(genre);
    }
}