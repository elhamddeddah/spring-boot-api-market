package com.market.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.SessionScope;

import com.market.entity.Post;
import com.market.model.Datatables;
import com.market.repository.PostRepository;
import com.market.service.PostService;

@RestController
@RequestMapping(value = "/dt")

public class PostWebController {
	@Autowired
	private PostRepository postRepository;
	
	List<Post> posts;

	@RequestMapping(value = "/my/url/list")
	public ResponseEntity listAllTable(@RequestParam("draw") int draw,
	                                  @RequestParam("start") int start,
	                                  @RequestParam("length") int length) {
		System.out.println("posts : "+posts);
		 if (posts==null)
			 posts=postRepository.findAll();
	    int page = start / length; //Calculate page number
	   
	    Pageable pageable = PageRequest.of(
	            page,
	            length 
	    ) ;
	   
		start = (int)pageable.getOffset();
		final int end = Math.min((start + pageable.getPageSize()), posts.size());
		final Page<Post> responseData = new PageImpl<>(posts.subList(start, end), pageable, posts.size());
				
	    		
	    Datatables dataTable = new Datatables();

	    dataTable.setData(responseData.getContent());
	    dataTable.setRecordsTotal(responseData.getTotalElements());
	    dataTable.setRecordsFiltered(responseData.getTotalElements());

	    dataTable.setDraw(draw);
	    dataTable.setStart(start);

	    return ResponseEntity.ok(dataTable);

	}
}
