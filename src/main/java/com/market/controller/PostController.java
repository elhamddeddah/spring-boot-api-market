package com.market.controller;


import java.util.List;

import com.market.entity.Post;
import com.market.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//mark class as Controller
@RestController
@RequestMapping(value = "/post")
public class PostController
{
    //autowire the PostService class
    @Autowired
    PostService postService;
    //creating a get mapping that retrieves all the post detail from the database
    @GetMapping()
    private List<Post> getAllPost()
    {
        return postService.getAllPost();
    }
    //creating a get mapping that retrieves the detail of a specific post
    @GetMapping("/{postid}")
    private Post getPost(@PathVariable("postid") int postid)
    {
        return postService.getPost(postid);
    }
    //creating a delete mapping that deletes a specified post
    @DeleteMapping("/{postid}")
    private void deleteBook(@PathVariable("postid") int postid)
    {
        postService.deletePost(postid);
    }
    //creating post mapping that post the post detail in the database
    @PostMapping()
    private Post saveBook(@RequestBody Post post)
    {

        return postService.updatePost(post);
    }
    //creating put mapping that updates the post detail
    @PutMapping()
    private Post update(@RequestBody Post post)
    {
        return postService.createPost(post);
    }
}