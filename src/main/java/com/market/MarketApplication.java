
package com.market;
import com.market.entity.Genre;
import com.market.entity.Post;
import com.market.repository.GenreRepository;
import com.market.repository.PostRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

@SpringBootApplication
public class MarketApplication  extends SpringBootServletInitializer implements  CommandLineRunner  {


	//TODO hiden reject article from UP USER
	// CHANGE NAME PRODUCT TO ARICLE
	@Autowired
	PostRepository postRepository;
	@Autowired
	GenreRepository genreRepository;




	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MarketApplication.class);

	}

	public static void main(String[] args)  {
			SpringApplication.run(MarketApplication.class, args);
			
	}


	
	@Override
	public void run(String... args) throws Exception {
//		Genre genre1=Genre.builder().name(generateRandomString(10)).build();
//		Genre genre2=Genre.builder().name(generateRandomString(10)).build();
//	  Genre genre3=Genre.builder().name(generateRandomString(10)).build();
//	  List<Genre> genres=new ArrayList<Genre>();  
//	  genres.add(genre1);
//	  genres.add(genre2);
//	  genres.add(genre3);
//	  genreRepository.save(genre1);
//	  genreRepository.save(genre2);
//	  genreRepository.save(genre3);
//      List<String> list1=new ArrayList<String>();  
//	  list1.add("/images/1.jpg");  
//	  list1.add("/images/2.jpg");  
//	  list1.add("/images/275366009_502613804770587_8476234767423160186_n.jpg");  
//	  list1.add("/images/bg4.jpg");  
//	  list1.add("/images/bg555.jpg");  
//	  list1.add("/images/bg0.jpg");  
//	  list1.add("/images/telechargement.jpg");  
//	  list1.add("/images/qr_op.PNG"); 
//	  list1.add("/images/frame3.png"); 
//	  list1.add("/images/frame2.png"); 
//	  list1.add("/images/frame1.png"); 
//	  list1.add("/images/frame.png"); 
//	  list1.add("/images/bg.png");  
//	  for (int i = 0; i <8; i++) {
//		    	 int price=generateRandomInt(1000,500000);
//		    	 int discount=generateRandomInt(0,50);
//		    	 int num=generateRandomInt(1000000,9999999);
//		    	// (create_date_time, desc, genre_id, is_new, oldprice, photos, price, qte, tel, update_date_time) 
//		    	 String tel=Math.random()<0.5 ? "2"+num : "3"+num ;
//		    	// while (tel.length()<8) tel+=tel.charAt(3);
//		    	 Post post=Post.builder()
//		 				.price(price)
//		 				.tel(tel)	
//		 		.detail(generateRandomString(10)+" "+generateRandomString(8)+" "+generateRandomString(4)+" "+generateRandomString(8))
//		 				.qte(generateRandomInt(1,100) )
//		 				.isNew(Math.random()<0.5 ? true : false)
//		 				.likes(generateRandomInt(1,5000))
//		 				.oldprice((int)(price*(100+discount)/100))
//		 				.photos(list1.get(generateRandomInt(0,12)))
//		 				.genre(genres.get(generateRandomInt(0,2)))
//		 				.build();
//		    	 System.out.println(post);
//		 		postRepository.save(post);
				
	//}
		 

	}
	public static String generateRandomString(int len) {
		String chars = "ABCDbEFGHIJKLMNOPQRSTUVWXYZ";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(chars.charAt(rnd.nextInt(chars.length())));
		return sb.toString();
	}
	public static int generateRandomInt(int min,int max) {
		
		return  (int) (Math.random()*(max-min+1)+min);
	}
}
